{
  "parser": "babel-eslint",
  "globals": {
    "require": true,
    "module": true,
    "process": true,
    "__dirname": true
  },
  "env": {
    "browser": true
  },
  "plugins": [
    "react"
  ],
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module",
    "ecmaFeatures": {
      "module": true,
      "jsx": true,
      "experimentalObjectRestSpread": true
    }
  },
  "rules": {
    "consistent-this": 2,                     // enforces consistent naming when capturing the current execution context
    "quotes": [2, "single",
      { "allowTemplateLiterals": true }],
    "eqeqeq": 2,                              // require the use of === and !==
    "no-bitwise": 2,                          // disallow use of bitwise operators (off by default)
    "no-console": 1,                          // disallow use of console
    "no-debugger": 2,                         // disallow use of debugger
    "no-plusplus": 2,                         // disallow use of unary operators
    "no-empty": 1,                            // disallow empty statements
    "no-nested-ternary": 2,                   // disallow nested ternary expressions
    "no-unused-vars": 1,                      // disallow declaration of variables that are not used in the code
    "no-use-before-define": [2,               // disallow use of variables before they are defined
      { "functions": false, "classes": true }],
    "no-undef": 2,                            // disallow use of undeclared variables unless mentioned in a /*global */ block
    "no-new": 2,                              // disallow use of new operator when not part of the assignment or comparison
    "no-loop-func": 0,                        // disallow creation of functions within loops
    "no-mixed-spaces-and-tabs": 2,            // disallow mixed spaces and tabs for indentation
    "no-unused-expressions": 2,               // disallow usage of expressions in statement position
    "no-proto": 2,                            // disallow usage of __proto__ property
    "space-before-blocks": [1, "always"],     // require space before blocks
    "max-len": [1, 140],                      // specify the maximum length of a line in your program
    "max-params": [2, 3],                     // limits the number of parameters that can be used in the function declaration
    "max-depth": [2, 2],                      // specify the maximum depth that blocks can be nested
    "react/jsx-uses-vars": [2],               // Prevent variables used in JSX to be incorrectly marked as unused (jsx-uses-vars)
    "react/jsx-key": [1],                     // Detect missing key prop (jsx-key)
    "react/jsx-uses-react": [2],              // Prevent React to be incorrectly marked as unused
    "react/display-name": 0,
    "react/jsx-boolean-value": 1,
    "react/jsx-no-undef": 1,
    "react/jsx-sort-prop-types": 0,
    "react/jsx-sort-props": 0,
    "react/no-multi-comp": 1,
    "react/no-unknown-property": 1,
    "react/react-in-jsx-scope": 1,
    "react/self-closing-comp": 1
  }
}
