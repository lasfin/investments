const path = require('path');
const webpack = require('webpack');
const contentBase = path.resolve(__dirname, 'dist');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin')


const develop = process.env.NODE_ENV !== 'production';

const extractStyles = new ExtractTextPlugin({
    filename: develop ? 'css/[name].css': 'css/[name].[contenthash:8].css'
});

const plugins = [
    new CleanWebpackPlugin(['dist']),
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    }),
    extractStyles,
    new HtmlWebpackPlugin({
        template: 'src/index.html',
        minify: {
            collapseWhitespace: true,
            removeRedundantAttributes: true,
            useShortDoctype: true,
            removeEmptyAttributes: true,
            removeStyleLinkTypeAttributes: true,
            keepClosingSlash: true
        }
    }),
];

if (!develop) {
    plugins.push(
        new UglifyJsPlugin({
            sourceMap: true,
            parallel: true
        })
    )
}

module.exports = {
    entry: './src/index.js',
    output: {
        filename: develop ? 'app.js' : 'app.[chunkhash:8].min.js',
        path: contentBase
    },
    plugins,
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                loader: 'eslint-loader',
                enforce: 'pre',

            },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['@babel/preset-env', {
                                'targets': {
                                    'browsers': ['last 2 versions', 'safari >= 7']
                                }
                            }],
                            '@babel/preset-react'
                        ],
                        plugins: [
                            '@babel/plugin-proposal-object-rest-spread',
                            'transform-class-properties'
                        ],
                        cacheDirectory: true
                    }
                }
            },
            {
                test: /\.scss$/,
                use: extractStyles.extract({
                    use: [{
                        loader: 'css-loader',
                        options: {
                            minimize: true,
                            modules: true,
                            sourceMap: true
                        }
                    }, {
                        loader: 'sass-loader'
                    }],
                    fallback: 'style-loader'
                })
            }
        ]
    },
    watchOptions: {
        aggregateTimeout: 300,
        poll: true,
        ignored: /node_modules/
    },
    devServer: {
        compress: true,
        port: 9000,
        open: true
    }
};
