import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Rules.scss';

class Rules extends Component {

    getSizeClassName = (base) => {
        return styles[base + this.props.size];
    };

    render() {
        return (
            <div className={styles.Rules}>
                <div className={`${styles.info} ${this.getSizeClassName('info')}`}>
                    { this.props.info }
                </div>
                <div className={`${styles.data} ${this.getSizeClassName('data')}`}>
                    { this.props.data }
                </div>
            </div>
        )
    }
}

Rules.propTypes = {
    size: PropTypes.oneOf(['S', 'M']).isRequired,
    info: PropTypes.string.isRequired,
    data: PropTypes.string.isRequired
};

export default Rules;
