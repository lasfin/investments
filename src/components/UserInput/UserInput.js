import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import throttle from 'lodash.throttle';
import styles from './UserInput.scss';


class UserInput extends Component {

    constructor() {
        super();
        this.onChange = throttle(this.onChange, 2000); // cause we don't want to change input immediately
        this.fetchPrice = throttle(this.fetchPrice, 1500);
    }

    onChange = (data) => {
        const value = data.floatValue || 0;
        this.props.onEditStop(value);
    };

    componentDidUpdate(prevProps) {
        if (prevProps.lots !== this.props.lots) {
            this.fetchPrice(this.props.lots);
        }
    }

    fetchPrice = (lots) => {
        this.props.getFullPrice(lots);
    };

    focus = () => {
        this.inputElem.focus();
    };

    render() {
        return (
            <div
                className={`${styles.UserInput} ${styles[this.props.extraCssClasses]}`}
                onClick={ this.focus }
            >
                <div className={`${styles.description}`}>
                    { this.props.description }
                </div>
                <NumberFormat
                    className={ styles.input }
                    getInputRef = {(el) => this.inputElem = el }
                    decimalScale = { this.props.decimalScale }
                    fixedDecimalScale = { false }
                    onValueChange = { this.onChange }
                    thousandSeparator=' '
                    decimalSeparator=','
                    value={ this.props.value ? this.props.value : '' }
                    allowNegative = { false }
                />
            </div>
        )
    }
}

UserInput.propTypes = {
    extraCssClasses: PropTypes.string,
    lots: PropTypes.number.isRequired,
    getFullPrice: PropTypes.func.isRequired,
    onEditStop: PropTypes.func.isRequired,
    description: PropTypes.string.isRequired,
    maxValue: PropTypes.number.isRequired,
    decimalScale: PropTypes.number.isRequired,
    lastInput: PropTypes.number.isRequired
};

export default UserInput;