import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import config from '../../config';
import styles from './SubmitOrder.scss';

class SubmitOrder extends Component {

    formatPrice = (price) => {
        return (price / 100).toString().replace('.', ',');
    };

    spinner = () => {
        return (
            <div className={styles.spinner}>
                <div className={styles.spinnerItem + ' ' + styles.bounce1}/>
                <div className={styles.spinnerItem + ' ' + styles.bounce2}/>
                <div className={styles.spinnerItem}/>
            </div>
        )
    };

    submitBtn = () => {
        return (
            <button
                className={`${styles.btn } ${!this.props.fullPrice && styles.btnDisable}`}>
                { this.props.loading ?
                    <div>
                        { this.spinner() }
                    </div>
                    :
                    <div>
                        { this.props.fullPrice ?
                            <span>
                                <span className={styles.sell}>Продать на</span>
                                { this.formatPrice(this.props.fullPrice) }
                                { config.currenciesMap[this.props.currency] }
                            </span>
                            :
                            <span>Продать</span>
                        }
                    </div>
                }
            </button>
        )
    };

    render() {
        return (
            <div>
                { this.submitBtn() }
                { Boolean(this.props.commission) &&
                    <div className={ styles.description }>
                        Включая комиссию {`${this.props.commission / 100} `}
                        { config.currenciesMap[this.props.commissionCurrency] }
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    currency: state.exchange.currency,
    commissionCurrency: state.exchange.commissionCurrency,
    commission: state.exchange.commission,
    loading: state.exchange.loading,
    fullPrice: state.exchange.fullPrice
});

SubmitOrder.propTypes = {
    currency: PropTypes.string.isRequired,
    commissionCurrency: PropTypes.string.isRequired,
    commission: PropTypes.number.isRequired,
    loading: PropTypes.bool.isRequired,
    fullPrice: PropTypes.number.isRequired
};

export default connect(mapStateToProps)(SubmitOrder);