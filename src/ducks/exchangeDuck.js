import Promise from 'pinkie-promise';
import initialState from './initialState';

const SET_LOTS = 'SET_LOTS';
const SET_PRICE = 'SET_PRICE';
const FETCH_PRICE = 'FETCH_PRICE';
const FETCHED_PRICE = 'FETCHED_PRICE';

export default function exchangeReducer(state = initialState.exchange, { type, payload }) {
    switch (type) {
        case SET_LOTS:
            const limitedLots = limitLots(payload, state.available);
            return {
                ...state,
                lots: limitedLots,
                price: state.lotPrice * limitedLots,
                lastInput: payload
            };
        case SET_PRICE:
            const cents = payload * 100;
            const lots = getLotsByPrice(cents, state.lotPrice, state.available);

            return {
                ...state,
                lots: lots,
                price: lots ? lots * state.lotPrice : cents,
                lastInput: payload
            };
        case FETCH_PRICE:
            return {
                ...state,
                loading: true,
                commission: 0,
                fullPrice: 0
            };
        case FETCHED_PRICE:
            if (payload.lots !== state.lots) {
                return {
                    ...state
                }
            }
            return {
                ...state,
                loading: false,
                commission: payload.commission.value,
                commissionCurrency: payload.commission.currency,
                fullPrice: payload.fullPrice.value,
                currency: payload.fullPrice.currency
            };
        default:
            return state;
    }
}


function limitLots(lots, available) {
    if (lots > available) {
        return available;
    }
    return lots;
}

function getLotsByPrice(price, lotPrice, available) {
    const remainder = price % lotPrice;
    const lots = (price - remainder) / lotPrice;
    return lots > available ? available : lots;
}


export function setLots(lots) {
    return {
        type: SET_LOTS,
        payload: lots
    };
}

export function setPrice(price) {
    return {
        type: SET_PRICE,
        payload: price
    }
}

function fetchPrice() {
    return {
        type: FETCH_PRICE
    }
}

function fetchedPrice(data) {
    return {
        type: FETCHED_PRICE,
        payload: data
    }
}

export function getFullPrice(lots = 0) {
    return function (dispatch) {
        dispatch(fetchPrice());
        return new Promise((resolve) => {
            if (lots === 0) {
                resolve({
                    lots,
                    fullPrice: { value: 0, currency: 'RUB' },
                    commission: { value: 0, currency: 'RUB' }
                });
            }
            setTimeout(() => {
                const fakeLotPrice = initialState.exchange.lotPrice;
                const commission = setFakeCommision(lots);
                resolve({
                    lots,
                    fullPrice: { value: lots * fakeLotPrice + commission, currency: 'RUB' },
                    commission: { value: commission, currency: 'RUB' }
                });
            }, (Math.floor(Math.random() * 3) + 1) * 1000)
        }).then((data) => {
            dispatch(fetchedPrice(data))
        });
    };
}

function setFakeCommision(lots) {
    return lots * 2 * 100;
}


