import { combineReducers } from 'redux';
import exchangeReducer from './exchangeDuck';

const rootReducer = combineReducers({
    exchange: exchangeReducer
});

export default rootReducer;
