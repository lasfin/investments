export default {
    exchange: {
        lots: 0,
        price: 0,
        lastInput: 0,
        lotRatio: 50,
        available: 930,
        lotPrice: 5711.4 * 100,
        loading: false,
        currency: 'RUB',
        commission: 0,
        commissionCurrency: 'RUB',
        fullPrice: 0
    }
}