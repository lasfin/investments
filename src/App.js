import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Rules from './components/Rules';
import UserInput from './components/UserInput';
import SubmitOrder from './components/SubmitOrder';
import config from './config';
import { setLots, setPrice, getFullPrice } from './ducks/exchangeDuck';
import { connect } from 'react-redux';
import styles from './App.scss';

class App extends Component {

    render() {
        return (
            <div className={styles.app}>
                <Rules
                    info={`1 лот = ${this.props.lotRatio} акций`}
                    data={`${(this.props.lotPrice / 100).toString().replace('.', ',')} ` +
                          `${config.currenciesMap[this.props.currency]}`}
                    size='M'
                />
                <Rules
                    info='Доступно для продажи'
                    data={`${this.props.available} лотов`}
                    size='S'
                />
                <UserInput
                    description="Количество лотов"
                    extraCssClasses="roundedLeft"
                    onEditStop={ this.props.setLots }
                    getFullPrice={ this.props.getFullPrice }
                    lots={ this.props.lots }
                    value={ this.props.lots }
                    maxValue={ this.props.available }
                    decimalScale={ 0 }
                    lastInput = { this.props.lastInput }
                />
                <UserInput
                    description={`Сумма продажи, ${config.currenciesMap[this.props.currency]}`}
                    extraCssClasses="roundedRight"
                    onEditStop={ this.props.setPrice }
                    getFullPrice={ () => {} }
                    lots = { this.props.lots }
                    value={ this.props.price / 100 }
                    maxValue={ this.props.available * this.props.lotPrice / 100 }
                    decimalScale = { 2 }
                    lastInput= { this.props.lastInput }
                />
                <SubmitOrder/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    lots: state.exchange.lots,
    price: state.exchange.price,
    lotRatio: state.exchange.lotRatio,
    available: state.exchange.available,
    lotPrice: state.exchange.lotPrice,
    currency: state.exchange.currency,
    lastInput: state.exchange.lastInput
});

const mapDispatchToProps = (dispatch) => ({
    setLots: (lots) => dispatch(setLots(lots)),
    setPrice: (price) => dispatch(setPrice(price)),
    getFullPrice: (lots) => dispatch(getFullPrice(lots))
});

App.propTypes = {
    lots: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
    lotRatio: PropTypes.number.isRequired,
    available: PropTypes.number.isRequired,
    lotPrice: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
    setLots: PropTypes.func.isRequired,
    setPrice: PropTypes.func.isRequired,
    getFullPrice: PropTypes.func.isRequired,
    lastInput: PropTypes.number.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
